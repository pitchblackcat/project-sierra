﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
abstract class CompositeJob : Job
{
    protected Queue<Job> jobs = new Queue<Job>();
    private Job job;

    protected override bool Start(Actor actor) { return true; }
    protected override void Do(Actor actor)
    {
        if (job == null) {
            if (jobs.Count == 0) Finish(actor);
            else StartNextJob();
        }

        job.Update(actor);
    }

    private void StartNextJob()
    {
        this.job = jobs.Dequeue();
        this.job.OnFinished += FinishJob;
    }

    private void FinishJob(Actor actor)
    {
        this.job.OnFinished -= FinishJob;
        this.job = null;

        if (jobs.Count == 0)
            Finish(actor);
    }
}

