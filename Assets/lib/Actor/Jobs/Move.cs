﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class Move : Job
{
    private Vector3 point;

    public Move(Vector3 point) {
        this.point = point;
    }

    protected override bool Start(Actor actor)
    {
        return actor.agent.SetDestination(point);
    }

    protected override void Do(Actor actor) 
    {
        if (HasReachedDestination(actor)) Finish(actor);
    }

    private bool HasReachedDestination(Actor actor)
    {
        return actor.agent.remainingDistance < .1f;
    }
}

