﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Job {
    public delegate void Finished(Actor actor);
    public event Finished OnFinished;

    public string type;
    protected bool doing;
    protected bool finished;

    public bool Update(Actor actor) {
        if (this.doing && !this.finished) this.Do(actor);

        if (!this.doing) {
            this.doing = this.Start(actor);
            Logger.Debug(Channel.Job, string.Format("{0} started doing {1}", actor.gameObject.name, this.GetType().Name));
        }

        return doing;
    }

    abstract protected bool Start(Actor actor);
    abstract protected void Do(Actor actor);

    public bool IsDoing() {
        return this.doing;
    }

    public void Finish(Actor actor)
    {
        Logger.Debug(Channel.Job, string.Format("{0} finished doing {1}", actor.gameObject.name, this.GetType().Name));
        this.finished = true;
        if (OnFinished != null) OnFinished(actor);
    }
}
