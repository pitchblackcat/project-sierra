﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Actor : MonoBehaviour {
    public delegate void Finished();
    public event Finished OnFinished;

    private NavMeshAgent _agent;
    public NavMeshAgent agent { get { return _agent; } }

    [SerializeField]
    protected Job job;

	// Use this for initialization
    protected virtual void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
    protected virtual void Update()
    {
        if (this.job == null) return;
        this.job.Update(this);
	}

    public bool HasJob()
    {
        return this.job != null;
    }

    public virtual bool SetJob(Job job) {
        if (this.job != null) return false;
        this.job = job;
        this.job.OnFinished += Finish;
        return true;
    }

    private void Finish(Actor actor)
    {
        this.job.OnFinished -= Finish;
        this.job = null;
        if (this.OnFinished != null) this.OnFinished();
    }
}
