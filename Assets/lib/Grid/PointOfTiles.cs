﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfTiles : GroupOfTiles
{
    public Vector3Int point;

    public override List<Vector3Int> GetTiles()
    {
        List<Vector3Int> list = new List<Vector3Int>();
        list.Add(this.point);
        return list;
    }
}
