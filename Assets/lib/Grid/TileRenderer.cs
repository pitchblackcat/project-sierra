﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRenderer : MonoBehaviour {
    private MeshRenderer _renderer;
    public Color color;
	
	void Start () {
        this._renderer = GetComponentInChildren<MeshRenderer>();
        this.SetColor(this.color);
	}

    public void SetColor(Color color) {
        if (this._renderer)
        {
            this._renderer.material.SetColor("_Color", color);
        }
    }
}
