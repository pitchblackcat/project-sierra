﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Grid : SingletonMonoBehaviour<Grid> {

    public TileRenderer tileRendererTemplate;

    public Vector2Int mapSize = new Vector2Int(50, 50);
    public Vector2Int tileSize = new Vector2Int(1, 1);
    public bool drawGrid = true;
    public int drawGridDistance = 4;

    public int h { get { return Mathf.RoundToInt(mapSize.y * tileSize.y); } }
    public int w { get { return Mathf.RoundToInt(mapSize.x * tileSize.x); } }

    public Vector3Int bl { get { return Vector3Int.zero; } }
    public Vector3Int br { get { return Vector3Int.right * w; } }
    public Vector3Int tl { get { return new Vector3Int(0,0,1) * h; } }
    public Vector3Int tr { get { return br + tl; } }

    private Queue<TileRenderer> tileRendererBuffer = new Queue<TileRenderer>();
    private Dictionary<Vector3Int, TileRenderer> activeTileRenderers = new Dictionary<Vector3Int, TileRenderer>();

    public List<GroupOfTiles> tiles = new List<GroupOfTiles>();

    protected virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(bl, br);
        Gizmos.DrawLine(br, tr);
        Gizmos.DrawLine(tr, tl);
        Gizmos.DrawLine(tl, bl);
    }

    // Update is called once per frame
    void Update()
    {
        DrawGrid();
    }

    public Vector3 RoundPoint(Vector3 worldPoint)
    {
        return TileToWorldPoint(WorldToTilePoint(worldPoint));
    }

    public Vector3Int WorldToTilePoint(Vector3 worldPoint)
    {
        return new Vector3Int(
            Mathf.RoundToInt(worldPoint.x / tileSize.x),
            0,
            Mathf.RoundToInt(worldPoint.z / tileSize.y)
        );
    }

    public Vector3 TileToWorldPoint(Vector3Int tilePoint)
    {
        return new Vector3(
            tilePoint.x * tileSize.x,
            0,
            tilePoint.z * tileSize.y
        );
    }

    private void DrawGrid()
    {
        Dictionary<Vector3Int, TileRenderer> newTileRenderers = new Dictionary<Vector3Int, TileRenderer>();

        foreach (GroupOfTiles group in this.tiles.FindAll(shape => shape.visible))
        {
            foreach (Vector3Int point in group.GetTiles())
            {
                if (newTileRenderers.ContainsKey(point)) continue;

                TileRenderer tile = this.activeTileRenderers.ContainsKey(point) ? this.activeTileRenderers[point] : NewTileRenderer();
                newTileRenderers.Add(point, tile);
                tile.transform.position = this.TileToWorldPoint(point);
            }
        }

        foreach (Vector3Int point in this.activeTileRenderers.Keys.Except(newTileRenderers.Keys).ToList())
        {
            var renderer = this.activeTileRenderers[point];
            this.activeTileRenderers.Remove(point);
            RemoveTileRenderer(renderer);
        }

        this.activeTileRenderers = newTileRenderers;
    }

    private void RemoveTileRenderer(TileRenderer tileRenderer)
    {
        tileRenderer.gameObject.SetActive(false);
        this.tileRendererBuffer.Enqueue(tileRenderer);
    }

    private TileRenderer NewTileRenderer()
    {
        TileRenderer tileRenderer = this.tileRendererBuffer.Any() ? this.tileRendererBuffer.Dequeue() : GameObject.Instantiate<TileRenderer>(tileRendererTemplate);
        tileRenderer.transform.parent = this.transform;
        tileRenderer.transform.localScale = new Vector3(tileSize.x, 1, tileSize.y);
        tileRenderer.gameObject.SetActive(true);
        return tileRenderer;
    }
}