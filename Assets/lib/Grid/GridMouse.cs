﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMouse : SingletonMonoBehaviour<GridMouse> {

    private PointOfTiles _shape = new PointOfTiles();

    void Start() {
        Grid.instance.Value += grid => grid.tiles.Add(_shape);
    }

	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Grid.instance.Value += grid => {
                this._shape.point = grid.WorldToTilePoint(hit.point);
            };            
        }
	}
}
