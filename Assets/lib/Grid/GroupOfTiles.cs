﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GroupOfTiles {
    public bool visible = true;
    abstract public List<Vector3Int> GetTiles();
}
