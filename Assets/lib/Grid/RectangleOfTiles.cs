﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectangleOfTiles : GroupOfTiles
{
    public Vector2Int bottomLeft;
    public Vector2Int topRight;

    public override List<Vector3Int> GetTiles()
    {
        List<Vector3Int> tiles = new List<Vector3Int>();
        for (int x = bottomLeft.x; x < topRight.x; x++)
        {
            for (int y = bottomLeft.y; y < topRight.y; y++)
            {
                tiles.Add(new Vector3Int(x, 0, y));
            }
        }
        return tiles;        
    }
}
