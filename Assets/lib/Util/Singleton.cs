﻿using System;
using UnityEngine;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
{
    private static T _instance;
    public static T instance
    {
        get {
            if (_instance == null) throw new Exception("Singleton " + typeof(T).Name + " is not set yet!");
            return _instance; 
        }
        set
        {
            _instance = value;
        }
    }

    // Use this for initialization
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Logger.Fatal(Channel.Singleton, "Double singleton detected!");
            Destroy(gameObject);
        }

        _instance = (T)this;
        Logger.Info(Channel.Singleton, typeof(T).Name + " just awoke");
    }
}
