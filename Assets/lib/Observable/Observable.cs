﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Observable<T>
{
    public delegate void Observe(T t);

    public event Observe OnNext;
    public event Observe OnComplete;

    private T _value;
    public T value
    {
        get { return this._value; }
    }

    public virtual void Next(T t)
    {
        _value = t;
        if (OnNext != null) OnNext(t);
    }

    public virtual void Complete(T t)
    {
        if (OnComplete != null) OnComplete(t);
        this.ResetNext();
        this.OnComplete = null;
    }

    protected void ResetNext() {
        this.OnNext = null;
    }
}