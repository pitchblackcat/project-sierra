﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Async<T>
{
    public delegate void Observe(T t);

    private Observe val;
    public event Observe Value
    {
        add {
            if (_value != null) {
                value(_value);
                return;
            }

            val -= value;
            val += value; 
        }
        remove { val -= value; }
    }

    private T _value;

    public void Set(T t) {
        if (this._value != null) {
            throw new Exception(string.Format("Async of type {0} is already set!", typeof(T).Name));
        }

        this._value = t;
        if (val != null)
        {
            val(t);
            val = null;
        }        
    }

    public T Get() {
        return this._value;
    }
}