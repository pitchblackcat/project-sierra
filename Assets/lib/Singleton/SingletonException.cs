﻿using System;
using UnityEngine;

public class SingletonException<T> : Exception
{
    public static string NOT_INSTANTIATED = "Singleton {0} is not instantiated yet!";
    public static string DOUBLY_INSTANTIATED = "Singleton {0} is instantiated twice!";

    public SingletonException(string message) : base(string.Format(message, typeof(T).Name))
    { 

    }

    public string getTypeName() {
        return typeof(T).Name;
    }
}

public class SingletonNotInstantiatedException<T> : SingletonException<T>
{
    public SingletonNotInstantiatedException() : base(SingletonException<T>.NOT_INSTANTIATED) { }
}

public class SingletonDoublyInstantiatedException<T> : SingletonException<T>
{
    public SingletonDoublyInstantiatedException() : base(SingletonException<T>.DOUBLY_INSTANTIATED) { }
}
