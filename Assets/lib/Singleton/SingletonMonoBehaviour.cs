﻿using System;
using UnityEngine;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
{
    public static Async<T> instance = new Async<T>();

    // Use this for initialization
    void Awake()
    {
        T inst = instance.Get();
        if (inst != null && inst != this)
        {
            throw new SingletonDoublyInstantiatedException<T>();
            Destroy(gameObject);
        }

        instance.Set((T)this);
        try
        {
            Logger.Info(Channel.Singleton, typeof(T).Name + " just awoke");
        }
        catch (SingletonNotInstantiatedException<Logger> e) 
        {}
    }
}
