﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger : SingletonMonoBehaviour<Logger>
{
    public static void Bulk(Priority priority, Channel channel, string title, List<string> messages) { 
        messages.ForEach((message) => {
            message = string.Format("[{0}] {1}", title, message);
            Logger.instance.Value += logger => logger.Log(priority, channel, message);
        }); 
    }
    public static void Debug(Channel channel, string message)
    {
        Logger.instance.Value += logger => logger.Log(Priority.Debug, channel, message);
    }
    public static void Info(Channel channel, string message)
    {
        Logger.instance.Value += logger => logger.Log(Priority.Info, channel, message);
    }
    public static void Warn(Channel channel, string message)
    {
        Logger.instance.Value += logger => logger.Log(Priority.Warning, channel, message);
    }
    public static void Error(Channel channel, string message)
    {
        Logger.instance.Value += logger => logger.Log(Priority.Error, channel, message);
    }
    public static void Fatal(Channel channel, string message)
    {
        Logger.instance.Value += logger => logger.Log(Priority.FatalError, channel, message);
    }

    public Dictionary<Channel, ChannelSettings> activeChannels = new Dictionary<Channel, ChannelSettings>() {
        {Channel.Singleton, new ChannelSettings() { color = Color.blue, priority = Priority.Info }},
        {Channel.Job, new ChannelSettings() { color = Color.white, priority = Priority.Warning }}
    };

    private void Log(Priority priority, Channel channel, string message) {
        if (activeChannels.ContainsKey(channel))
        {
            ChannelSettings settings = activeChannels[channel];
            string finalMessage = FormatMessage(priority, channel, settings, message);
            // Call the correct unity logging function depending on the type of error 
            switch (priority)
            {
                case Priority.FatalError:
                    UnityEngine.Debug.LogError(finalMessage);
                    UnityEngine.Debug.Break();
                    break;

                case Priority.Error:
                    if (settings.priority <= Priority.Error) UnityEngine.Debug.LogError(finalMessage);
                    break;

                case Priority.Warning:
                    if (settings.priority <= Priority.Warning) UnityEngine.Debug.LogWarning(finalMessage);
                    break;

                case Priority.Info:
                case Priority.Debug:
                    if (settings.priority <= priority) UnityEngine.Debug.Log(finalMessage);
                    break;
            }
        }
    }

    private string FormatMessage(Priority priority, Channel channel, ChannelSettings settings, string message)
    {
        string channelColor = ColorUtility.ToHtmlStringRGB(settings.color);
        string priortiyColor;
        switch (priority) {
            case Priority.FatalError:
            case Priority.Error:
                priortiyColor = "red";
                break;
            case Priority.Warning:
                priortiyColor = "orange";
                break;
            default:
                priortiyColor = "black";
                break;
        }
        return string.Format("<b><color=#{0}>[{1}] </color></b> <color={2}>{3}</color>", channelColor, channel, priortiyColor, message);
    }
}

[System.Serializable]
public struct ChannelSettings {
    public Priority priority;
    public Color color;
}
