﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum Channel
{
    Singleton,
    Job
}

public enum Priority : uint
{
    // Default, simple output about game
    Off = 4,
    // Default, simple output about game
    Debug = 4,
    // Default, simple output about game
    Info = 3,
    // Warnings that things might not be as expected
    Warning = 2,
    // Things have already failed, alert the dev
    Error = 1,
    // Things will not recover, bring up pop up dialog
    FatalError = 0,
}
