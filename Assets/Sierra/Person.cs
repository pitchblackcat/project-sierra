﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class Person {
    public delegate void Destroyed();
    public event Destroyed OnDestroyed;

    public const string Available = "Available";
    public const string Unavailable = "Unavailable";

    public string name;
    public string state;
}
