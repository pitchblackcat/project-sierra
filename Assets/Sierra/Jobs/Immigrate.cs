﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Immigrate : Move {

    private House house;
    private Person person;

    public Immigrate(House house, Person person) : base(house.transform.TransformPoint(house.door)) {
        this.house = house;
        this.person = person;

        house.people.Add(person);
        person.state = Person.Unavailable;
        OnFinished += OnJobFinished;
        house.OnDestroyed += OnHouseDestroyed;
    }

    void OnHouseDestroyed()
    {
    }

    void OnJobFinished(Actor actor)
    {
        person.state = Person.Available;
    }
}
