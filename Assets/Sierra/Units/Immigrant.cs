﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Immigrant : Actor {
    override public bool SetJob(Job job)
    {
        bool set = base.SetJob(job);
        if (set) {
            OnFinished += () => GameObject.Destroy(gameObject);
        }
        return set;
    }
}
