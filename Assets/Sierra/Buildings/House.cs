﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class House : Building {
    public int maxInhabitants = 5;
    public List<Person> people = new List<Person>();

    void Start() {
        MinisterOfHousing.instance.AddHouse(this);
    }

    public bool HasSpace() {
        return people.Count < maxInhabitants;
    }
}
