﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSpawn : Building {
    public Animal template;

    public int maxAnimals = 5;
    public float wanderDiameter = 1f;
    public List<Animal> animals = new List<Animal>();

    public Ticker ticker;

    protected void Start()
    {
        ticker.OnTick += SpawnAnimals;
    }

	protected void Update () {
        ticker.Update();
	}

    protected virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.TransformPoint(door), wanderDiameter);
    }

    private void SpawnAnimals() {
        if (animals.Count >= maxAnimals) return;
        Animal animal = GameObject.Instantiate<Animal>(template);
        animal.transform.position = transform.TransformPoint(door);
        MakeAnimalWander(animal);
        animals.Add(animal);
    }

    void MakeAnimalWander(Animal animal) {
        Vector3 vec = Quaternion.AngleAxis(Random.Range(-180, 180), Vector3.up) * Vector3.forward * Random.Range(0, wanderDiameter);
        Move move = new Move(transform.TransformPoint(door + vec));
        if (animal.SetJob(move)) {
            animal.OnFinished += () => MakeAnimalWander(animal);
        }
    }
}
