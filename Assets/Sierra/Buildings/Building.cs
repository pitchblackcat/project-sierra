﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : MonoBehaviour {
    public delegate void Destroyed();
    public event Destroyed OnDestroyed;

    public Vector3 door;

    protected virtual void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.TransformPoint(door), .5f);
    }

    protected virtual void OnDestroy() {
        if (OnDestroyed != null) {
            OnDestroyed();
        }

        OnDestroyed = null;
    }
}
