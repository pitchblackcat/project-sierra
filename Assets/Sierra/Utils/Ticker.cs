﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Ticker {
    public delegate void Tick();
    public event Tick OnTick;

    public int delay = 100;
    private int _counter = 0;
	
	public void Update () {
        this._counter = --this._counter % this.delay;
        if (this._counter == 0 && this.OnTick != null) this.OnTick();
	}
}
