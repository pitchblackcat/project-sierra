﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plaything : MonoBehaviour {
    public List<Actor> actors = new List<Actor>();

	// Update is called once per frame
	void Update () {
        MoveOnClick();
	}

    private void MoveOnClick() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetButtonDown("Fire2"))
        {
            if (Physics.Raycast(ray, out hit, 100))
            {
                Actor actor = actors.Find(a => !a.HasJob());
                if (actor == null) return;

                actors.Remove(actor);
                actors.Add(actor);

                Grid.instance.Value += grid => actor.SetJob(new Move(grid.RoundPoint(hit.point)));
            }
        }
    }
}
