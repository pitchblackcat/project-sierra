﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Minister<T> : MonoBehaviour where T : Minister<T>
{
    public static T instance;

    // Use this for initialization
    void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);

        if (instance == null)
            instance = (T)this;
    }    
}
