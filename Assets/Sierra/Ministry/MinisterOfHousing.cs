﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class MinisterOfHousing : Minister<MinisterOfHousing> {

    public Immigrant villagerTemplate;
    public List<Vector3> spawnPoints = new List<Vector3>();
    public Ticker immigrantSpawnTicker = new Ticker();
    
    private int availablePeople = 10;
    private List<Person> people = new List<Person>();
    private List<House> houses = new List<House>();

    void Start() {
        immigrantSpawnTicker.OnTick += SendImmigrant;
    }

    void Update () {
        immigrantSpawnTicker.Update();
	}

    private void SendImmigrant() {
        House house = houses.Find(h => h.HasSpace());
        if (house == null) return;

        Immigrant immigrant = GameObject.Instantiate<Immigrant>(villagerTemplate);
        immigrant.transform.position = GetClosestSpawnPoint(house.transform.position);
        immigrant.SetJob(new Immigrate(house, CreatePerson()));
    }

    private Vector3 GetClosestSpawnPoint(Vector3 target) {
        return spawnPoints
            .OrderBy(spawnPoint => Vector3.Distance(spawnPoint, target))
            .ElementAt(0);
    }

    private Person CreatePerson() {
        Person p = new Person();

        people.Add(p);
        return p;
    }

    public void AddHouse(House house) {
        houses.Add(house);
        house.OnDestroyed += () => houses.Remove(house);
    }
}
