﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MinisterOfHousing)), CanEditMultipleObjects]
public class MinisterOfHousingEditor : Editor {
    protected virtual void OnSceneGUI()
    {
        MinisterOfHousing minister = (MinisterOfHousing) target;
        EditorGUI.BeginChangeCheck();
        foreach (Vector3 point in minister.spawnPoints)
        {
            Vector3 newPoint = Handles.PositionHandle(point, Quaternion.identity);
            int index = minister.spawnPoints.IndexOf(point);
            minister.spawnPoints[index] = newPoint;
        }
        EditorGUI.EndChangeCheck();
    }

    [DrawGizmo(GizmoType.Selected | GizmoType.Active)]
    public static void OnDrawGizmosSelected(MinisterOfHousing minister, GizmoType gizmoType)
    {
        Gizmos.color = Color.yellow;
        foreach (Vector3 point in minister.spawnPoints)
        {
            Gizmos.DrawWireSphere(point, .5f);
        }
    }
}
