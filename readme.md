The birth of Project Sierra
I started on Project Sierra because he was in a nostalgic mood one night and decided to install the good ol’ game Pharaoh: Cleopatra. After playing it for a couple hours he noticed the game’s mechanics weren’t all that complicated, and started thinking about implementing them himself. He booted up his long time pall Unity3D and Project Sierra saw the light.

About
Project Sierra is currently a hobby project, led by a single man, with the intend of blossoming into a full blown game, if it can attract a solid team of hobbyist game developers of every trait. As it’s currently a hobby project, no money is involved.

Aim of the game
The aim of the game is to create a fun and engaging city builder with lots of different types of buildings that will unlock as you progress through the levels.

You will need to create housing for your people by designating spots on a grid based map. People will move into your city and build crappy huts to live in. By providing your subjects with the right resources and amenities they will upgrade their housing, provinding additional livingspace and making the residents more wealthy.

These resources and amenities will be provided by the workplaces the player places down in strategic locations, minimizing travel distances and maximizing the amount of houses that will enjoy it’s produce.

All the wealth and fame of building a city attracts attention though; some of the surrounding cities will want to trade while others might like to seize it for their own. Be prepared to defend your city by training a capable army.

As you rule a vain kingdom, the game will ultimate revolve towards building a grandiose monument to finish a level. These monuments will require a lot of money and resources to build and multiple types of workers to finish it.

To win a level, the player will fulfill a number of objectives, having to do with population, wealth or the construction of a monument. It might also involve keeping relations high with a needy allie.

Game mechanics
Citybuilding
Project Sierra is a city builder at heart. The player will be able to pick buildings from a menu a plop them down on the map. Taking heavy inspiration from the Sierra Citybuilders, there will be a number of categories of buildings with ample buildings to choose from. The difficulty and variation in levels will often limit the available buildings.

Building type categories are:

housing
food production
food distribution
entertainment
hygiene
infrastructure
raw resource production
monuments
religious(?)
military
beautification
Pawns
The player might be able to plan out the city and give direct orders to the military, but it’s the pawns that run things down there.

Pawns will autonamously move around the map, doing your bidding.
They have a designated house that they will periodically visit to recharge their stats.

Pawns have individual stats, like sleepiness, hunger and wealth.

Pawns with a job will move to live at the nearest available housing.
They will travel to their jobs, and only when at their job they will be producing, making logistics a big part of the game.

Oftentimes resources need to be hauled from one place to another. Pawns will move to one location to pick up goods and deliver them at the destination, again puting emphasis on logistics planning.

Often they will have to go inside of a building for a prolonged time, to either work or recharge there. That’s when they will turn into nothing but a number, so the game can manage a bustling city with many people without it crawling to a halt.

Economy
Project Sierra aims to have a number of economy systems in place. You will be able to trade, create and collect wealth from your subjects by tax.

Subject needs
Your subjects have needs. subjects of higher stature will have higher needs to fullfil.

They will have a plathora of needs, like:

food water and a roof over their heads. Without these they will get sick and - - a job, without it they will steal to provide for their family
entertainment, the higher the stature, the more they need
religion
security
hygiene
healthcare
etc
Upset subjects might decide to take to the streets and start riotting.

Military
Build barracks and train pawns to get access to soldiers.
Levy your troops to command them RTS style.

Trading
Some resources required for the level are simply not there to produce.
Trade with allies to aquire those precious resources, but don’t upset them, or they might close their borders

diplomacy
Dicplomacy will be a meta system in the game. Allied cities will periodically make requests based on the levels storyline. You will have to try to fullfil them. Granting their request will lead to an increase in relations, while failing to do so will lower it. Get it low enough and the city will close it’s trading to you. Possibly failing the mission by locking out required resources.